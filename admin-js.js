function confirmAction(){
	jQuery( "#dialog-approve-confirm" ).dialog({
		resizable: false,
		
		modal: true,
		open: function( event, ui ) {
			jQuery('.ui-dialog-titlebar').css('display','none');
		}
		});
}

function confirmActionDelete() {
	
	jQuery( "#dialog-delete-confirm" ).dialog({
		resizable: false,
		
		modal: true,
		open: function( event, ui ) {
			jQuery('.ui-dialog-titlebar').css('display','none');
		}
		});

}

function cancelConfirmActionDelete() {
	
	jQuery( "#canceled-dialog-delete-confirm" ).dialog({
		resizable: false,
		
		modal: true,
		open: function( event, ui ) {
			jQuery('.ui-dialog-titlebar').css('display','none');
		}
		});

}

function confirmActionUndoDelete() {
	
	jQuery( "#dialog-undo-delete-confirm" ).dialog({
		resizable: false,
		
		modal: true,
		open: function( event, ui ) {
			jQuery('.ui-dialog-titlebar').css('display','none');
		}
		});

}



jQuery(function(){
	// Undo Delete confirmation
	jQuery('.undo-del-link').click(function(){
		jQuery('.undo-del-link').removeClass('ugurl');
		jQuery(this).addClass('ugurl');
		});
	
	jQuery("#undoDeleteDialogYes").click(function(){
  	  	jQuery( "#dialog-undo-delete-confirm" ).dialog( "close" );
  	  	 
 		jQuery("#hansRowlinksForm").attr('action',jQuery(".ugurl").attr('alt'));
 		jQuery('#hansRowlinksForm').submit();
    	});
	
	jQuery("#undoDeleteDialogClose").click(function(){
	  	jQuery( "#dialog-undo-delete-confirm" ).dialog( "close" );
	  	});
	
	// Delete confirmation
	jQuery('.del-link').click(function(){
		jQuery('.del-link').removeClass('gurl');
		jQuery(this).addClass('gurl');
		});
	
	jQuery("#deleteDialogYes").click(function(){
  	  	jQuery( "#dialog-delete-confirm" ).dialog( "close" );
  	  	 
 		jQuery("#hansRowlinksForm").attr('action',jQuery(".gurl").attr('alt'));
 		jQuery('#hansRowlinksForm').submit();
    	});
	
	jQuery("#deleteDialogClose").click(function(){
	  	jQuery( "#dialog-delete-confirm" ).dialog( "close" );
	  	});
	
	// canceled reservation Delete confirmation
	jQuery("#cancelDeleteDialogYes").click(function(){
  	  	jQuery( "#cancel-dialog-delete-confirm" ).dialog( "close" );
  	  	 
 		jQuery("#hansRowlinksForm").attr('action',jQuery(".gurl").attr('alt'));
 		jQuery('#hansRowlinksForm').submit();
    	});
	
	jQuery("#cancelDeleteDialogClose").click(function(){
		
	  	jQuery("#canceled-dialog-delete-confirm").dialog( "close" );
	  	});
	
	// Approve confirmation
	jQuery('.app-link').click(function(){
		jQuery('.app-link').removeClass('gurl');
		jQuery(this).addClass('gurl');
	});
	
	jQuery("#approveDialogYes").click(function(){
  	  	jQuery( "#dialog-approve-confirm" ).dialog( "close" );
  	  	jQuery("#hansRowlinksForm").attr('action',jQuery(".gurl").attr('alt'));
 		jQuery('#hansRowlinksForm').submit();
    	});
	
	jQuery("#approveDialogClose").click(function(){
	  	jQuery( "#dialog-approve-confirm" ).dialog( "close" );
	  	});
	
	
	// Send email confirmation on make reservation page
	jQuery("#make-reservation-confirmation-email").click(function(){
		jQuery( "#dialog-email-confirmation" ).dialog({
			resizable: false,
			height:140,
			modal: true,
			});
		});
	
	jQuery("#emailConfirmationYes").click(function(){
  	  	jQuery( "#dialog-email-confirmation" ).dialog( "close" );
  	  	jQuery("#make-reservation-confirmation-email").attr('checked',true);
 		});
	
	jQuery("#emailConfirmationClose").click(function(){
	  	jQuery( "#dialog-email-confirmation" ).dialog( "close" );
	  	jQuery("#make-reservation-confirmation-email").attr('checked',false);
	  	});
});

function hansrowlinks(val,url)
{
	jQuery(document).ready(function(){
	if(val==='Approve'){
		jQuery("#confirm-reservation").val(val);
		jQuery("#edit-reservation").val(null);
		jQuery("#delete-reservation").val(null);
		confirmAction();
			
	}else if(val==='Edit'){
		jQuery("#edit-reservation").val(val);
		jQuery("#confirm-reservation").val(null);
		jQuery("#delete-reservation").val(null);
		jQuery("#hansRowlinksForm").attr('action',url);  
		jQuery('#hansRowlinksForm').submit();
	}else if(val==='undoDelete'){
		jQuery("#undo-delete-reservation").val(val);
		jQuery("#delete-reservation").val(null);
		jQuery("#edit-reservation").val(null);
		jQuery("#confirm-reservation").val(null);
		confirmActionUndoDelete();
		}
	});
}	

function sortList(sortType, sortField, bookingType, url)
{
//	alert(0);
	jQuery(document).ready(function(){
		
		var form = document.createElement("form");
	    form.setAttribute("method", 'post');
	    form.setAttribute("action", url);
	    
	    var hiddenField = document.createElement("input");
	    hiddenField.setAttribute("type", "hidden");
	    hiddenField.setAttribute("name", 'sortType');
	    hiddenField.setAttribute("value", sortType);
	    form.appendChild(hiddenField);
	    
	    var hiddenField = document.createElement("input");
	    hiddenField.setAttribute("type", "hidden");
	    hiddenField.setAttribute("name", 'sortField');
	    hiddenField.setAttribute("value", sortField);
	    form.appendChild(hiddenField);
	    
	    var hiddenField = document.createElement("input");
	    hiddenField.setAttribute("type", "hidden");
	    hiddenField.setAttribute("name", 'bookingType');
	    hiddenField.setAttribute("value", bookingType);
	    form.appendChild(hiddenField);
	       
	    document.body.appendChild(form);
	    form.submit();
	    
	});
}	

function searchSortList(sortType, sortField, url)
{
//	alert(sortField);
	jQuery(document).ready(function(){
		
		var form = document.createElement("form");
	    form.setAttribute("method", 'post');
	    form.setAttribute("action", url);
	    
	    var hiddenField = document.createElement("input");
	    hiddenField.setAttribute("type", "hidden");
	    hiddenField.setAttribute("name", 'searchSortType');
	    hiddenField.setAttribute("value", sortType);
	    form.appendChild(hiddenField);
	    
	    var hiddenField = document.createElement("input");
	    hiddenField.setAttribute("type", "hidden");
	    hiddenField.setAttribute("name", 'searchSortField');
	    hiddenField.setAttribute("value", sortField);
	    form.appendChild(hiddenField);
	    
	    document.body.appendChild(form);
	    form.submit();
	    
	});
}	


function hansrowlinksDelete(val,url)
{
	if(val==='Delete'){
		jQuery("#delete-reservation").val(val);
		jQuery("#edit-reservation").val(null);
		jQuery("#confirm-reservation").val(null);
		confirmActionDelete();

	}
}

function hansrowlinksUndoDelete(val,url)
{
	if(val==='undoDelete'){
		jQuery("#undo-delete-reservation").val(val);
		jQuery("#delete-reservation").val(null);
		jQuery("#edit-reservation").val(null);
		jQuery("#confirm-reservation").val(null);
		confirmActionUndoDelete();

	}
}

function cancelhansrowlinksDelete(val,url)
{
	if(val==='Delete'){
		jQuery("#delete-reservation").val(val);
		jQuery("#edit-reservation").val(null);
		jQuery("#confirm-reservation").val(null);
		cancelConfirmActionDelete();

	}
}

function ExportCSV(url, startTime, finishTime, date){
	method = "post";
    
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", url);

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", 'exp_export_button');
    hiddenField.setAttribute("value", 'clicked');
    form.appendChild(hiddenField);
    
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", 'exp_start_time');
    hiddenField.setAttribute("value", startTime);
    form.appendChild(hiddenField);
    
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", 'exp_finish_time');
    hiddenField.setAttribute("value", finishTime);
    form.appendChild(hiddenField);
    
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", 'exp_date');
    hiddenField.setAttribute("value", date);
    form.appendChild(hiddenField);
    
 
    
    document.body.appendChild(form);
    form.submit();
    
}

function __resetTimeFilter(month, year,type) {
	jQuery("#confirmed-time-filter-start-time").val(null);
	jQuery("#confirmed-time-filter-finish-time").val(null);
	document.forms['confirmed-time-filter-form'].submit();
}

function __changeMonth(month, year,type) {
    method = "post";
                                                                                            									    
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", '');

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", 'res_change_month');
    hiddenField.setAttribute("value", '1000');
    form.appendChild(hiddenField);
                                                                                                                                    
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", 'inc_type');                                                
    if(type==1)
    {
        hiddenField.setAttribute("value", '+');
    }else
    {
        hiddenField.setAttribute("value", '-');
    }                                                                                        
    form.appendChild(hiddenField);
                                                                                            
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", 'res_current_month');
    hiddenField.setAttribute("value", month);
    form.appendChild(hiddenField);


    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", 'res_current_year');
    hiddenField.setAttribute("value", year);
    form.appendChild(hiddenField);                                                                                                                               							     
                                                                                            									
    document.body.appendChild(form);
    form.submit();
}

function __post_to_url(params,url) {
    method = "post";                                                                                                                                                                                											    
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", url);                                                                                                                                                                                																					    
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", 'key');
    hiddenField.setAttribute("value", params);                                                                                                                                                                                									
    form.appendChild(hiddenField);                                                                                          											
    document.body.appendChild(form);
    form.submit();
}

function hansheadersubmitform(val)
{
	if(val=='next'){
		 jQuery("#hans-next-date-submit").val(val);
		 document.headerDate.submit();
	}else if(val=='prev'){
		jQuery("#hans-previous-date-submit").val(val);
		jQuery("#hans-next-date-submit").val(null);
		 document.headerDate.submit();
		}
}	
/**/
jQuery(function(){
	jQuery('.assign-table').click(function(){
		
		jQuery('.assign-table').removeClass('selectedRow');
		jQuery(this).addClass('selectedRow');
		
		var res_id = jQuery('.selectedRow').attr('data-res-id');
		var admin_url = jQuery('.selectedRow').attr('data-admin-url');
		
		jQuery.ajax({  
        	  type: 'POST',  
        	  url: admin_url,  
        	  dataType: 'html',
        	  data: {  
        	  action: 'hansGetTables',
            	res_id: res_id
            	
            	},  
              beforeSend: function() {
            	  loadingHtml = jQuery('.selectedRow').html();
            	jQuery('.selectedRow').html("<img src='/wp-content/plugins/reservation/js/images/loading.gif' />");
            	},	
            	complete: function(){
            		jQuery('.selectedRow').html(loadingHtml);
                    },
        	  success: function(data, textStatus, XMLHttpRequest){  
        		  
        		  
        		  
        		  jQuery( "#assign-table-container" ).dialog({
        				
        	            modal: true,
        	            zIndex: 9999,
        	            maxWidth:600,
        	            maxHeight: 600,
        	            width: 360,
        	            
        	            top:100,
        	            open: function( event, ui ) {
        	            	
        	    			jQuery('#assign-table-container').html(data);
        	    			jQuery('#assign-table-container').append("<script>jQuery(function(){ jQuery('.avTable').click(function(){jQuery('.avTable').removeClass('selectedTab'); jQuery(this).addClass('selectedTab'); 	var avTables = jQuery('.avTable');	var selectedTable = jQuery(this); avTables.css('color','black'); selectedTable.css('color','red'); var avbl_seats = selectedTable.attr('data-avbl-seats'); var res_guests = jQuery('.selectedRow').attr('data-res-guests');  /*if start */ if(parseInt(avbl_seats)>=parseInt(res_guests)){ jQuery('.selectedRow').html( selectedTable.attr('data-tbl-number')); /*start AJAX*/ var admin_url = jQuery('.selectedRow').attr('data-admin-url'); jQuery.ajax({ type: 'POST', url: admin_url, data: { action: 'hansAssignTableAjax', selectedTableNo: selectedTable.attr('data-table-id'), selectedTableNoResID: jQuery('.selectedRow').attr('data-res-id') }, success: function(data, textStatus, XMLHttpRequest){ jQuery('#assign-table-container' ).dialog( 'close' ); }, error: function(MLHttpRequest, textStatus, errorThrown){ /*alert(errorThrown);*/ } }); /*end if*/}  /* end click*/});    });</script>");
        	    			jQuery('#assign-table-container').append("<script>jQuery(function(){jQuery('.avTable').mouseover(function(){ jQuery(this).find('span').text(jQuery(this).attr('data-avbl-seats')); 	jQuery(this).css('background','#eeeeee'); }).mouseout(function(){ jQuery(this).find('span').text(jQuery(this).attr('data-tbl-number'));	jQuery(this).css('background','#ffffff');});});</script>");
        	    			jQuery('#assign-table-container').append("<script>jQuery(function(){ /*START*/jQuery('.clearButton').click(function(){ jQuery.ajax({ type: 'POST', url: jQuery('.selectedRow').attr('data-admin-url'), data: {  action: 'hansAssignTableAjax', selectedTableNo: '', selectedTableNoResID: jQuery('.selectedRow').attr('data-res-id')  }, success: function(data, textStatus, XMLHttpRequest){  jQuery('#assign-table-container' ).dialog( 'close' ); jQuery('.selectedRow').html('NA');  },   error: function(MLHttpRequest, textStatus, errorThrown){ /*alert(errorThrown);*/ } }); });/*END*/ });</script>");
        	    			
        	    		},
        	            close: function( event, ui ) {
        	    
        	            }
        	            
        	        });
            		
        	  },  
        	  error: function(MLHttpRequest, textStatus, errorThrown){  
        	  /*alert(errorThrown);*/  
        	  }  
        	  });
		
		
		
		
	});
	
	
jQuery('.assign-table-make').click(function(){
		
		jQuery('.assign-table').removeClass('selectedRow');
		jQuery(this).addClass('selectedRow');
		
		var res_id = jQuery('.selectedRow').attr('data-res-id');
		var admin_url = jQuery('.selectedRow').attr('data-admin-url');
		
		jQuery.ajax({  
        	  type: 'POST',  
        	  url: admin_url,  
        	  dataType: 'html',
        	  data: {  
        	  action: 'hansGetTables',
            	res_id: res_id
            	
            	},  
        	beforeSend: function() {
          	  loadingHtml = jQuery('.selectedRow').html();
          	jQuery('.selectedRow').html("<img src='/wp-content/plugins/reservation/js/images/loading.gif' />");
          	},	
          	complete: function(){
          		jQuery('.selectedRow').html(loadingHtml);
                  },
        	  success: function(data, textStatus, XMLHttpRequest){  
        		  
        		  jQuery( "#assign-table-container" ).dialog({
        				
        	            modal: true,
        	            zIndex: 9999,
        	            maxWidth:600,
        	            maxHeight: 600,
        	            width: 360,
        	            
        	            top:100,
        	            open: function( event, ui ) {
        	            	
        	    			jQuery('#assign-table-container').html(data);
        	    			jQuery('#assign-table-container').append("<script>jQuery(function(){ jQuery('.avTable').click(function(){jQuery('.avTable').removeClass('selectedTab'); jQuery(this).addClass('selectedTab'); 	var avTables = jQuery('.avTable');	var selectedTable = jQuery(this); avTables.css('color','black'); selectedTable.css('color','red'); jQuery('.assign-table-make').html( selectedTable.attr('data-tbl-number') ); jQuery('#make-reservation-table-number').attr('value',  selectedTable.attr('data-table-id') ); jQuery('#assign-table-container' ).dialog( 'close' );  /*end click*/});    /*end function*/});</script>");
        	    			jQuery('#assign-table-container').append("<script>jQuery(function(){jQuery('.avTable').mouseover(function(){ jQuery(this).find('span').text(jQuery(this).attr('data-avbl-seats')); 	jQuery(this).css('background','#eeeeee'); }).mouseout(function(){ jQuery(this).find('span').text(jQuery(this).attr('data-tbl-number'));	jQuery(this).css('background','#ffffff');});});</script>");
        	    			jQuery('#assign-table-container').append("<script>jQuery(function(){ /*START*/jQuery('.clearButton').click(function(){ jQuery('.assign-table-make').html( 'NA' ); jQuery('#make-reservation-table-number').attr('value', '' ); jQuery('#assign-table-container' ).dialog( 'close' ); });/*END*/ });</script>");
        	    			
        	    		},
        	            close: function( event, ui ) {
        	    
        	            }
        	            
        	        });
            		
        	  },  
        	  error: function(MLHttpRequest, textStatus, errorThrown){  
        	  /*alert(errorThrown);*/  
        	  }  
        	  });
		
		
		
		
	});
});

function preload(arrayOfImages) {
    jQuery(arrayOfImages).each(function(){
        jQuery('<img/>')[0].src = this;
        // Alternatively you could use:
        // (new Image()).src = this;
    });
}

function del(url)
{
    jQuery('#btn_del').attr('onclick', "dodell('"+url+"')");
    jQuery( "#del-popup" ).dialog({
        modal: true,
        zIndex: 9999,
        maxWidth:358,
        maxHeight: 150,
        width: 300,
        
        open: function( event, ui ) {
			jQuery('.ui-dialog-titlebar').css('display','none');
		},
        close: function( event, ui ) {
        }
    });
}
function dodell(url)
{
    window.location=url;
} 
